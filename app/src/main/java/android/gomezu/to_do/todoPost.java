package android.gomezu.to_do;

public class todoPost {

    public String title;
    public String due;
    public String date;
    public String category;

    public todoPost(String title, String due, String date, String category){
        this.title = title;
        this.due = due;
        this.date = date;
        this.category = category;
    }
}
